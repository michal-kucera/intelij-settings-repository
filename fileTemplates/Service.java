#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
#parse("Log imports.java")
import org.springframework.stereotype.Service;
#parse("File Header.java")
@Service
class ${NAME}ServiceImpl implements ${NAME}Service {

    private static final Logger logger = getLogger(${NAME}ServiceImpl.class);

    ${NAME}ServiceImpl() {
    }
}
