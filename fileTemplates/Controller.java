#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
#parse("Log imports.java")
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
#parse("File Header.java")

@Validated
@RestController
class ${NAME}Controller {

    private static final Logger logger = getLogger(${NAME}Controller.class);
}
