import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

#parse("File Header.java")
@RunWith(MockitoJUnitRunner.class)
public class ${NAME} {

    @InjectMocks
    private ${CLASS_NAME} underTest;
${BODY}
}
