#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
#parse("File Header.java")
@Validated
@Component
@ConfigurationProperties(prefix = "")
public class ${NAME}Properties {

}
