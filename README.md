# IntellIj Idea Settings Repository

## Update Settings Repository
1. Disable `IDE Settings Sync` (`Ctrl + Shift + A`, type `IDE Settings Sync` and click on `OFF`)
2. Go to `File | Manage IDE Settings | Settings Repository..`
3. Add the url to Settings Repository (`git@gitlab.com:michal.kucera/intelij-settings-repository.git`)
4. Click on `Overwrite Remote`
5. Enable `IDE Settings Sync` (`Ctrl + Shift + A`, type `IDE Settings Sync` and click on `ON`) so the Settings Repository will get automatically synced when you made a change

## Apply settings from Settings Repository
1. Go to `File | Manage IDE Settings | Settings Repository..`
2. Add the url to Settings Repository (`git@gitlab.com:michal.kucera/intelij-settings-repository.git`)
3. Click on `Overwrite Local`

## Update plugins list
1. Install [plugin-importer-exporter](https://github.com/shiraji/plugin-importer-exporter) plugin
2. Go to `File | Export plugins`

## Apply plugins list
1. Install [plugin-importer-exporter](https://github.com/shiraji/plugin-importer-exporter) plugin
2. Go to `File | Import plugins` and select `/custom-settings/plugins.json` file

## Apply Save Actions settings
1. Go to `.idea` folder of the project
2. Paste there the `/custom-settings/saveactions_settings.xml` file
3. Restart IDE
